package main

import (
	"fmt"
	"log"
)

type ErrAccessTokenInvalid struct {
	Url     string
	Code    int
	Message string
}

func (eati ErrAccessTokenInvalid) Error() string {
	return fmt.Sprintf(
		"invalid access token - url: '%s', response code: %d, response: '%s'",
		eati.Url,
		eati.Code,
		eati.Message,
	)
}

type ErrRefreshTokenInvalid struct {
	Url     string
	Code    int
	Message string
}

func (erti ErrRefreshTokenInvalid) Error() string {
	return fmt.Sprintf(
		"invalid refresh token - url: '%s', response code: %d, response: '%s'",
		erti.Url,
		erti.Code,
		erti.Message,
	)
}

type ErrAuthCodeInvalid struct {
	Url     string
	Code    int
	Message string
}

func (eaci ErrAuthCodeInvalid) Error() string {
	return fmt.Sprintf(
		"invalid auth code - url: '%s', response code: %d, response: '%s'",
		eaci.Url,
		eaci.Code,
		eaci.Message,
	)
}

func runAccessToken(url string, succeed bool) (string, error) {
	if succeed {
		return "Success!", nil
	}
	return "", ErrAccessTokenInvalid{url, 401, "access token expired"}
}

func runRefreshToken(url string, succeed bool) (string, error) {
	if succeed {
		return "Success!", nil
	}
	return "", ErrRefreshTokenInvalid{url, 400, "access token invalid"}
}

func runAuthCode(url string, succeed bool) (string, error) {
	if succeed {
		return "Success!", nil
	}
	return "", ErrAuthCodeInvalid{url, 400, "authorization code expired"}
}

func main() {
	resourceUrl := "https://somesite.com/resource/1"
	tokenUrl := "https://somesite.com/oauth/token"
	_, err := runAccessToken(resourceUrl, false)
	if err != nil {
		log.Printf("could not access resource: %v\n", err)
	}
	_, err = runRefreshToken(tokenUrl, false)
	if err != nil {
		log.Printf("could not refresh access token: %v\n", err)
	}
	_, err = runAuthCode(tokenUrl, false)
	if err != nil {
		log.Printf("could not exchange auth code: %v\n", err)
	}
}
